---
layout: markdown_page
title: "Category Direction - Importers"
description: "This GitLab product category is about enabling adoption of GitLab offerings (SaaS, Dedicated, self-managed) by bring existing projects to GitLab, or copying GitLab groups and projects to a different location. Find more information here!"
canonical_path: "/direction/manage/import_and_integrate/importers/"
---

- TOC
{:toc}

## Importers

| | |
| --- | --- |
| Stage | [Manage](/direction/manage/) |
| Maturity | N/A |
| Content Last Reviewed | `2024-01-09` |

### Introduction and how you can help

Thanks for visiting this category direction page on Importers in GitLab. This page belongs to the [Import and Integrate](https://about.gitlab.com/handbook/product/categories/#import-and-integrate-group) group of the Manage stage and is maintained by the group's Product Manager, [Magdalena Frankiewicz](https://gitlab.com/m_frankiewicz) ([E-mail](mailto:mfrankiewicz@gitlab.com), [Calendly](https://calendly.com/gitlab-magdalenafrankiewicz/45mins)).

This direction page is a work in progress, and everyone can contribute:
 
- Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Category%3AImporters&first_page_size=100) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?state=opened&page=1&sort=start_date_desc&label_name[]=Category:Importers) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and want to discuss how GitLab can improve Importers, we'd especially love to hear from you.

### Strategy and Themes

<!-- Describe your category. Capture the main problems to be solved in market (themes). Describe how you intend to solve these with GitLab (strategy). Provide enough context that someone unfamiliar with the details of the category can understand what is being discussed. -->

<%= partial("direction/manage/import_and_integrate/importers/templates/overview") %>

### 1 year plan
<!--
1 year plan for what we will be working on linked to up-to-date epics. This section will be most similar to a "road-map". Items in this section should be linked to issues or epics that are up to date. Indicate relative priority of initiatives in this section so that the audience understands the sequence in which you intend to work on them.
 -->

<%= partial("direction/manage/import_and_integrate/importers/templates/next") %>

#### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
or to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to epics encompass a vision that is a longer horizon and don't lay out an iteration plan. -->
This is a look ahead for the next iteration (about 3 months) that we have planned for the category:

- [Improve user contributions mapping in direct transfer](https://gitlab.com/groups/gitlab-org/-/epics/12378)
- [Bring direct transfer to General Availability](https://gitlab.com/groups/gitlab-org/-/epics/11400)
- [Enable to retry not fully completed migration of a chosen project with direct transfer](https://gitlab.com/groups/gitlab-org/-/epics/9760)

#### What we are currently working on
<!-- Scoped to the current month. This section can contain the items that you choose to highlight on the kickoff call. Only link to issues, not Epics.  -->
These are highlights from the current month's planned work:

- [Import chosen relation between two GitLab projects with NDJSON file](https://gitlab.com/gitlab-org/gitlab/-/issues/425798)
- [Add migration stats API](https://gitlab.com/gitlab-org/gitlab/-/issues/435188)
- [Remove preselection and force the user to select the destination group](https://gitlab.com/gitlab-org/gitlab/-/issues/431571)
- [GitHub import rate limit: ensure we are using GitHub App that is owned by a GitHub Enterprise Cloud](https://gitlab.com/gitlab-org/gitlab/-/issues/432970)

#### What we recently completed
<!-- Lookback limited to 3 months. Link to the relevant issues or release post items. -->

- [Comprehensive list of items that failed to be imported](https://about.gitlab.com/releases/2023/11/16/gitlab-16-6-released/#comprehensive-list-of-items-that-failed-to-be-imported) *16.6*
- [Remove hardcoded time limit for migrations to complete](https://about.gitlab.com/releases/2023/12/21/gitlab-16-7-released/#remove-hardcoded-time-limit-for-migrations-to-complete) *16.7*
- [Comprehensive results of imports by direct transfer](https://about.gitlab.com/releases/2023/12/21/gitlab-16-7-released/#comprehensive-results-of-imports-by-direct-transfer) *16.7*
- Performance and reliability improvements to GitHub importer and GitLab migration by direct transfer in support of imports of large projects ([list of issues completed](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=closed&label_name%5B%5D=scaled%20migrations&first_page_size=20))
- [Enhanced migration from Bitbucket Server and Bitbucket Cloud to GitLab](https://about.gitlab.com/blog/2023/11/30/enhanced-migration-from-bitbucket-server-and-bitbucket-cloud-to-gitlab/)

#### What is Not Planned Right Now
<!--  Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand -->

Group Import continuously evaluates and updates the Importers' direction and roadmap. As part of that effort, new Importers such as Trello, CircleCI, Subversion and Azure DevOps (TFS) are being discussed. While these discussions may ultimately lead to the implementation of a new feature or a new Importer, none of them are being planned at this time.

Given our focus on migrating GitLab groups and project by direct transfer (general availability and beyond) we are not prioritizing work that is not in scope of that effort.

Group Import is not focused on the ability to regularly back up and restore your GitLab data, for example nightly backups of all your data. For more information on this use-case, please see the [Backup and Restore category direction page](https://about.gitlab.com/direction/geo/backup_restore/).

### Best in Class Landscape
<!-- Blanket description consistent across all pages that clarifies what GitLab means when we say "best in class" -->

BIC (Best In Class) is an indicator of forecated near-term market performance based on a combination of factors, including analyst views, market news, and feedback from the sales and product teams. It is critical that we understand where GitLab appears in the BIC landscape.

#### Key Capabilities 
<!-- For this product area, these are the capabilities a best-in-class solution should provide -->

This table provides a quick overview of what GitLab importers exist today and which most important objects they each support. This list is not exhaustive and the detailed information can be found on the [Importers documentation page](https://docs.gitlab.com/ee/user/project/import/).

[tanuki]: https://about.gitlab.com/ico/favicon-16x16.png "GitLab"
[tan2]: <i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:1.25em" aria-hidden="true">

| Import source                                                                                 | Repos       | MRs        | Issues     | Epics     | Milestones | Wiki       | Designs   | API <sup>*</sup> |
|-----------------------------------------------------------------------------------------------|-------------|------------|------------|-----------|------------|------------|-----------|------------|
| [<i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:1.0em" aria-hidden="true"></i> Group and project migration by direct transfer](https://docs.gitlab.com/ee/user/group/import/)           | ✅          | ✅          | ✅          | ✅       | ✅          | ✅         | ✅        | ✅         |
| [<i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:1.0em" aria-hidden="true"></i> Group migration with export files (deprecated)](https://docs.gitlab.com/ee/user/group/settings/import_export.html)     | ➖ | ➖         | ➖          | ✅       | ✅          | ➖         | ➖        | ✅         |
| [<i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:1.0em" aria-hidden="true"></i> Project migration with export files](https://docs.gitlab.com/ee/user/project/settings/import_export.html) | ✅ | ✅         | ✅          | ➖       | ✅          | ✅         | ✅        | ✅         |
| [GitHub](https://docs.gitlab.com/ee/user/project/import/github.html)                          | ✅          | ✅          | ✅          | ➖       | ✅          | ✅         | ➖        | ✅         |
| [Bitbucket Cloud](https://docs.gitlab.com/ee/user/project/import/bitbucket.html)              | ✅          | ✅          | ✅          | ➖       | ✅          | ✅         | ➖        | ❌         |
| [Bitbucket Server](https://docs.gitlab.com/ee/user/project/import/bitbucket_server.html)      | ✅          | ✅          | ❌          | ➖       | ❌          | ➖         | ➖        | ✅         |
| [Gitea](https://docs.gitlab.com/ee/user/project/import/gitea.html)                            | ✅          | ✅          | ✅          | ➖       | ✅          | ➖         | ➖        | ❌         |
| [Git (Repo by URL)](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html)          | ✅          | ✅          | ➖          | ➖       | ➖          | ➖         | ➖        | ❌         |
| [Manifest file](https://docs.gitlab.com/ee/user/project/import/manifest.html)                 | ✅          | ✅          | ➖          | ➖       | ➖          | ➖         | ➖        | ❌         |
| [CSV](https://docs.gitlab.com/ee/user/project/issues/csv_import.html)                         | ➖          | ➖          | ✅          | ➖       | ➖          | ➖         | ➖        | ❌         |
| [FogBugz](https://docs.gitlab.com/ee/user/project/import/fogbugz.html)                        | ➖          | ➖          | ✅          | ➖       | ➖          | ➖         | ➖        | ❌         |

* ✅ : Supported
* ❌ : Not supported
* ➖ : Not applicable

**_<sup>*</sup> This column indicates whether this importer is accessible via API, in addition to the UI._**

#### Roadmap
<!-- Key deliverables we're focusing on to build a BIC solution. List the epics by title and link to the epic in GitLab. Minimize additional description here so that the epics can remain the SSOT. -->

- [Improve user contributions mapping in direct transfer](https://gitlab.com/groups/gitlab-org/-/epics/12378)
- [Bring direct transfer to General Availability](https://gitlab.com/groups/gitlab-org/-/epics/11400)
- [Enable to retry not fully completed migration of a chosen project with direct transfer](https://gitlab.com/groups/gitlab-org/-/epics/9760)
- [Support migrations with direct transfer between offline instances](https://gitlab.com/groups/gitlab-org/-/epics/8985)
- [Standardize quality, performance, security, logging of importers](https://gitlab.com/groups/gitlab-org/-/epics/12363)
- [Show progress of direct transfer import](https://gitlab.com/groups/gitlab-org/-/epics/9765)
- [Import additional project resources](https://gitlab.com/groups/gitlab-org/-/epics/9319)
- [Import GitHub project into GitHub mirrored repo](https://gitlab.com/gitlab-org/gitlab/-/issues/214018)
- [Pre-migration checks and report](https://gitlab.com/groups/gitlab-org/-/epics/9766)

### Target Audience
<!--
List the personas (https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->

- [Delaney, Development Team Lead](https://about.gitlab.com/handbook/product/personas/#delaney-development-team-lead)
- [Allison, Application Ops](https://about.gitlab.com/handbook/product/personas/#allison-application-ops)
- [Dakota, Application Development Director](https://about.gitlab.com/handbook/product/personas/#dakota-application-development-director)

### Automating group and project import with Professional Services

While the long-term goal for the Import group is to provide all the GitLab importing capabilities needed by our customers in our application, we recognize that GitLab's current capabilities may not support specific migration scenarios. Often, we're not aware of these requirements until a large customer provides us with specific migration requirements.

GitLab [Professional Services](https://about.gitlab.com/services/) team uses [Congregate](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate) tool to orchestrate user, group, and project import API calls in order to help customers automate scaled migrations. With the feature of migrating groups and projects by direct transfer ready for production use at any scale, we will be able to substitute the part of Congregate handling migration on groups and projects.
